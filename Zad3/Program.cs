﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    class Program
    {

        static void Main(string[] args)
        {

            Program pro = new Program();
            pro.readData();

        }

        private void readData()
        {
            int testCount = Convert.ToInt32(Console.ReadLine());

            for (int t = 0; t < testCount; t++)
            {
                string[] nodeAndColor = Console.ReadLine().Split(' ');

                int nodeCount = Convert.ToInt32(nodeAndColor[0]);
                int colorCount = Convert.ToInt32(nodeAndColor[1]);

                int[,] matrix = new int[nodeCount, nodeCount];

                for (int i = 0; i < nodeCount; i++)
                {
                    string line = Console.ReadLine();
                    string[] cols = line.Split(' ');
                    for (int j = 0; j < nodeCount; j++)
                    {
                        matrix[i, j] = Convert.ToInt32(cols[j]);
                    }
                }

                Coloring c = new Coloring(matrix, nodeCount, colorCount);
                c.init();
                c.start();
                
            }

        }
    }

    class Coloring
    {
        int[,] matrix;
        int N;
        int k;
        int[] COLOR;

        public Coloring(int[,] matrix, int nodeCount, int colorCount)
        {
            this.matrix = matrix;
            this.N = nodeCount;
            this.k = colorCount;
        }

        public void init()
        {
            COLOR = new int[N];
            for (int i = 0; i < N; i++)
            {
                COLOR[i] = 0;
            }
        }

        public void start()
        {
            if (coloring(0))
            {
                output();
            }
            else
            {
                Console.WriteLine("NIE");
            }
            
        }

        bool coloring(int u)
        {

            if (u == N)
                return true;

            for (int i = COLOR[u] + 1; i <= k; i++)
            {

                bool ok = true;
                for (int j = 0; j < N; j++)
                {
                    if (matrix[u, j] == 1 && i == COLOR[j])
                    {
                        ok = false;
                        break;
                    }
                }

                if (ok)
                {
                    COLOR[u] = i;

                    if (coloring( u + 1))
                        return true;
                }              
            }

            COLOR[u] = 0;

            return false;
        }

        void output()
        {
            var output = string.Join(" ", COLOR);
            Console.WriteLine(output);
        }
    }
}
